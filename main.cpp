#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN

#include <tchar.h>
#include <windows.h>
#include <windowsx.h>
// #include <mmsystem.h>  // for PlaySound (along with winmm.lib codeclock reference libwinmm.a)
#include <stdlib.h> // for rand functions
#include <stdio.h>
#include <math.h>
#include "resource.h"
//DEFINES
#define WINDOW_CLASS_NAME "GDI_GAME_TEMPLATE"

#define WINDOW_WIDTH 800 // Size of Game
#define WINDOW_HEIGHT 600

#define GAME_SPEED 30

#define TEST_CARD 0 // initialisation state.

// MACROS

#define KEYDOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000 ) ? 1 : 0 )
#define KEYUP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000 ) ? 0 : 1 )

//GLOBALS

HWND game_window = NULL;  // global game window handle
HINSTANCE game_instance = NULL ; //global game instance handle
HDC game_dc = NULL;  //globale device context ( for GDI ) handle

// for double buffering
HDC back_dc = NULL;
HBITMAP back_bmp = NULL;
HBITMAP old_bmp = NULL;
RECT back_rect = {0,0,WINDOW_WIDTH, WINDOW_HEIGHT};


DEVMODE game_screen;   //global for full screen mode

//global pens and brushes

HPEN white_pen = CreatePen(PS_SOLID,1,RGB(255,255,255));
HPEN black_pen = CreatePen(PS_SOLID,1,RGB(0,0,0));

HBRUSH white_brush = CreateSolidBrush(RGB(255,255,255));
HBRUSH black_brush = CreateSolidBrush(RGB(0,0,0));

       // create a random rectangle
    HPEN temp_pen = NULL;
    HBRUSH temp_brush = NULL;

// All declatations go in here for STRUCTS etc.

RECT temp_rect; // used to hold rect info

char text[80]; // temporary test string for displays
int game_state = TEST_CARD ; // initial start case

HBITMAP g_logo, g_logo_mask;
HBITMAP g_logo_color;
BITMAP bm;

int y = 0; //  Test card line mover
// FUNCTION DECLARATIONS
typedef struct _star {
    float x, y;
    float vx, vy;
} star;

star* stars=NULL;

int num_stars = 150;

void GameInit(BOOL); // call with FALSE for window or TRUE for fullscreen
void GameQuit();
void GameMain();
void StarfieldInit();
void StarfieldShow();

/*  Windows procedure  */
/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    HDC hdc = NULL; // handle to device context.
    PAINTSTRUCT ps; // used in WM_PAINT;

    switch (message)                  /* handle the messages */
    {
        case WM_CREATE:
        {
         // Initialisation
         HDC logoMem, maskMem, negmaskMem;

        g_logo = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_LOGO));
        if (g_logo ==  NULL )
        {
            MessageBox(hwnd,"Could not load IDB_LOGO!!","Error", MB_OK | MB_ICONEXCLAMATION);
            break;
        }

        GetObject(g_logo,sizeof(BITMAP),&bm);
        g_logo_mask=CreateBitmap(bm.bmWidth,bm.bmHeight,1,1,NULL);
        g_logo_color=CreateBitmap(bm.bmWidth,bm.bmHeight,1,1,NULL);

        logoMem=CreateCompatibleDC(0);
        maskMem=CreateCompatibleDC(0);
        negmaskMem=CreateCompatibleDC(0);

        SelectObject(logoMem,g_logo);
        SelectObject(maskMem,g_logo_mask);
        SelectObject(negmaskMem,g_logo_color);

        SetBkColor(logoMem,RGB(0,0,0));
        BitBlt(maskMem,0,0,bm.bmWidth,bm.bmHeight,logoMem,0,0,SRCCOPY);
        BitBlt(logoMem,0,0,bm.bmWidth,bm.bmHeight,maskMem,0,0,SRCINVERT);

        BitBlt(negmaskMem,0,0,bm.bmWidth,bm.bmHeight,logoMem,0,0,SRCCOPY);
        BitBlt(negmaskMem,0,0,bm.bmWidth,bm.bmHeight,negmaskMem,0,0,DSTINVERT);

        DeleteDC(logoMem);
        DeleteDC(maskMem);
        DeleteDC(negmaskMem);

         return(0);
        } break;

        case WM_PAINT:
        {
            hdc = BeginPaint(hwnd,&ps);

            EndPaint(hwnd,&ps);
            return(0);

        } break;

        case WM_DESTROY:
            {
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            return(0);
            } break;

        default: break;                     /* for messages that we don't deal with */

    } // end switch

    return (DefWindowProc (hwnd, message, wParam, lParam));
} // end WindowsProcedure


// WINMAIN

/*  Make the class name into a global variable  */
//TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = WINDOW_CLASS_NAME;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

    game_instance =hThisInstance;  // save the global game instance handle

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    if (!(hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           WINDOW_CLASS_NAME,         /* Classname */
           _T("Code::Blocks Template Win32 Game App"),       /* Title Text */
           WS_POPUP | WS_VISIBLE, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           WINDOW_WIDTH,                 /* The programs width */
           WINDOW_HEIGHT,                 /* and height in pixels */
           NULL,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           )))
           return(0);

    //save the game window handle
    game_window=hwnd;

    GameInit(TRUE);
    StarfieldInit();

    /* Make the window visible on the screen */
    //ShowWindow (hwnd, nCmdShow);

    /* Run the message loop but use PeekMessage to retrieve messages */
    while (TRUE)
    {

        DWORD start_tick = GetTickCount();  // get initial count to keep game speed constant
        if (PeekMessage (&messages, NULL, 0, 0,PM_REMOVE))
        {

        if ( messages.message == WM_QUIT ) break;
            /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);

        } // end if

    ExtTextOut(back_dc,0,0,ETO_OPAQUE,&back_rect, "",0,0);  // THis makes the screen black quicker than the FillRect Below apparently !
    //FillRect(back_dc,&back_rect,black_brush);
    // Game call goes here.
    StarfieldShow();
    GameMain();

    BitBlt(game_dc,0,0,WINDOW_WIDTH,WINDOW_HEIGHT,back_dc,0,0,SRCCOPY);

    // Check for <ESCAPE KEY to quit game >
    if (KEYDOWN(VK_ESCAPE)) SendMessage(hwnd,WM_CLOSE,0,0);

    //wait for timer
    while ((GetTickCount() - start_tick ) < GAME_SPEED );

    } // end while

    GameQuit();
    free(stars);

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return(messages.wParam);
}

// BEGIN GAME CODE.

void StarfieldInit()
{
    stars = (star*)malloc(sizeof(star) * num_stars);
     int i;

    for (i = 0; i< num_stars; i++)
    {
     stars[i].x = rand() % WINDOW_WIDTH;
     stars[i].y = rand() % WINDOW_HEIGHT;
     stars[i].vy = rand() / (float)RAND_MAX * 5 +2;
     stars[i].vx = 0;
    }
}

void StarfieldShow()
{

    HDC bmmem;
    HBITMAP stars_bmp;
    HBITMAP save_bmp;
    RECT stars_rect = {0,0,WINDOW_WIDTH,WINDOW_HEIGHT};

    HPEN hpenstars, hpenOld;
    hpenstars = CreatePen(PS_SOLID, 2, RGB(128, 64, 255));

    int i;

    bmmem=CreateCompatibleDC(back_dc);
    stars_bmp =CreateCompatibleBitmap(back_dc, WINDOW_WIDTH, WINDOW_HEIGHT);
    save_bmp = (HBITMAP)SelectObject(bmmem,stars_bmp);

    hpenOld = (HPEN)SelectObject(bmmem, hpenstars);

    SetBkColor(bmmem,RGB(0,0,0)); // black background
    SetBkMode(bmmem,OPAQUE );
    ExtTextOut(bmmem,0,0,ETO_OPAQUE,&stars_rect, "",0,0);  // THis makes the screen black quicker than the FillRect Below apparently !

    for (i = 0; i < num_stars; i++ )
      {
        stars[i].y += stars[i].vy;
       if ( stars[i].y < WINDOW_HEIGHT )
         {
           // plot the point
        //	   SetPixel(bmmem,stars[i].x,stars[i].y,RGB(255,255,255));
	   //          LineTo(bmmem,0,40);
	    MoveToEx(bmmem,stars[i].x,stars[i].y,NULL);
        LineTo(bmmem, stars[i].x,stars[i].y+stars[i].vy);

         }
        else
	{
	   //move it
	   stars[i].y = 0;
	}
      }

    BitBlt(back_dc,0,0,WINDOW_WIDTH,WINDOW_HEIGHT,bmmem,0,0,SRCCOPY);

    SelectObject(bmmem, hpenOld);
    DeleteObject(hpenstars);

    SelectObject(bmmem,save_bmp);
    DeleteObject(stars_bmp);
    DeleteObject(save_bmp);

//    ReleaseDC(back_dc,bmmem);
    DeleteObject(bmmem);


}

void GameInit(BOOL fullscreen)
{
    srand(GetTickCount()); // random seed

    // temp change to full screen mode
    if (fullscreen)
    {
    ZeroMemory(&game_screen,sizeof(game_screen)); // clear out the DEVMOD struct
    game_screen.dmSize = sizeof(game_screen);
    game_screen.dmPelsWidth = WINDOW_WIDTH;
    game_screen.dmPelsHeight = WINDOW_HEIGHT;
    game_screen.dmBitsPerPel = 16;
    game_screen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

    ChangeDisplaySettings(&game_screen, CDS_FULLSCREEN);

    }

    game_dc = GetDC(game_window);  // Get the GDI device context

    // for double buffering
    back_dc = CreateCompatibleDC(game_dc);
    back_bmp =CreateCompatibleBitmap(game_dc, WINDOW_WIDTH, WINDOW_HEIGHT);
    old_bmp = (HBITMAP)SelectObject(back_dc,back_bmp);

    ShowCursor(FALSE);

} // End of GameInit

void GameQuit()
{
    //Delete Pens and Brushes
    DeleteObject(white_pen);
    DeleteObject(black_pen);
    DeleteObject(white_brush);
    DeleteObject(black_brush);
    DeleteObject(temp_brush);
    DeleteObject(temp_pen);
    //release the back buffer
    SelectObject(back_dc,old_bmp);
    DeleteObject(back_bmp);
    DeleteObject(back_dc);
    DeleteObject(g_logo_mask);
    DeleteObject(g_logo_color);
    DeleteObject(g_logo);
    //release the device contects ( for GDI ) from the game window.
    ReleaseDC(game_window,game_dc);

    // return to original display
    ChangeDisplaySettings(NULL,NULL);

} // End of Quite Game

void GameMain()
{
    HPEN hPen;
    // variables for the rectangle
    RECT rectangle,rc1;

    // variables for the logo graphics.
    BITMAP bm;
    HDC bmmem;
    HBITMAP savebm;
    double posx, posy;

    switch(game_state)
    {
      case TEST_CARD:
      {
         SetBkColor(back_dc,RGB(0,0,0)); // black background
         SetBkMode(back_dc,OPAQUE );

//            GetClientRect(game_window,&rectangle);
//            rc1=rectangle;

        HBRUSH rand_brush = CreateSolidBrush(RGB(rand() % 255,0,rand() % 255));

           rc1.right =800;
           rc1.bottom=590;
           rc1.top=550;
           rc1.left=0;
           //FillRect(back_dc,&rc1,black_brush);
           SetBkMode(back_dc,TRANSPARENT);
           SetBkColor(back_dc,RGB(0,0,0));
           SetTextColor(back_dc,RGB(rand() % 255,128,rand() % 255)); //  text
           DrawText(back_dc,TEXT("** THE GRID **"),-1,&rc1,DT_CENTER | DT_VCENTER | DT_SINGLELINE);

    DeleteObject(rand_brush);
  //         SetTextColor(back_dc,RGB(255,255,255)); //  text

          if ( y++ > 100) y = 90;

    bmmem=CreateCompatibleDC(back_dc);

    GetObject(g_logo,sizeof(bm),&bm);
    posx = ( WINDOW_WIDTH / 2 ) - ( bm.bmWidth / 2);
    posy = ( WINDOW_HEIGHT / 2 ) - ( bm.bmHeight / 2);

           SetBkColor(back_dc,RGB(0,0,0));
           SetTextColor(back_dc,RGB(255,255,255)); //  text
    savebm = (HBITMAP)SelectObject(bmmem,g_logo_color);
    BitBlt(back_dc,posx,posy,bm.bmWidth,bm.bmHeight,bmmem,0,0,SRCAND);

    (HBITMAP)SelectObject(bmmem,g_logo);
   BitBlt(back_dc,posx,posy,bm.bmWidth,bm.bmHeight,bmmem,0,0,SRCPAINT);


    SelectObject(bmmem, savebm);
    DeleteObject(temp_pen);
        DeleteObject(savebm);
    DeleteDC(bmmem);

      }  break;

    }  // end of switch
} // end of GameMain
